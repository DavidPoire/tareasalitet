#include <iostream>
#include <vector>
#include "Monstruo.h"
int main()
{
	std::vector<Monstruo*> roles;
	int choice;

	while (true)
	{
		std::cout << "Quimera(1) Sucubo(2) Minotauro(3) Go(0): ";
		std::cin >> choice;
		if (choice == 3)
			roles.push_back(new Minotauro);
		else if (choice == 1)
			roles.push_back(new Quimera);
		else if (choice == 2)
			roles.push_back(new Sucubo);
		else
			break;
	}
	for (int i = 0; i < roles.size(); i++)
		roles[i]->atack_type();
	for (int i = 0; i < roles.size(); i++)
		delete roles[i];
	return 0;
}