#pragma once
#include <iostream>
class Monstruo
{
public:
	virtual void atack_type() = 0;
};

class Quimera : public Monstruo
{
public:
	void atack_type()
	{
		std::cout << "Te come entero\n";
	}
};
class Sucubo : public Monstruo
{
public:
	void atack_type()
	{
		std::cout << "Te mata en tus sue�os\n";
	}
};
class Minotauro : public Monstruo
{
public:
	void atack_type()
	{
		std::cout << "Te aplasta\n";
	}
};