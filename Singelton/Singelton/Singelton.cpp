#include "Singelton.h"


Singelton* Singelton::instance = 0;
Singelton::Singelton()
{
	randNum = std::rand() % 100;
}


Singelton *Singelton::getInstance()
{
	if (instance == 0)
		instance = new Singelton();

	return instance;
}

void Singelton::printNum()
{
	std::cout << randNum << std::endl;
}

Singelton::~Singelton()
{
}
