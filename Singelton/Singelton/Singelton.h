#pragma once
#include <iostream>

class Singelton
{
public:
	static Singelton* getInstance();
	void printNum();

	~Singelton();

private:
	static Singelton* instance;
	int randNum;
	Singelton();

};

