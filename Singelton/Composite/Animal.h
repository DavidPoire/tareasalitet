#pragma once
#include <iostream>
#include <string>
#include <vector>
class Animal
{
public:

	virtual void tipo() = 0;
	Animal();
	~Animal();
};

class Perro : public Animal
{
	std::string canino = "Canino \n";
public:

	void tipo();
};

class Gato : public Animal
{
	std::vector<Animal*> hijos;
public:
	void add(Animal* gatin);
	void tipo();

};
